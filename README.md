# CI

GitLab CI tools and templates.

## Tools Included Here

| Name        | Function                   | License | Language |
| ----------- | -------------------------- | ------- | -------- |
| shellcheck  | shell script linter        | GPLv3   | Haskell  |
| gokart      | Go Static Security Scanner |         |          |
| Coming Soon |                            |         |          |

## Other Projects & CI Jobs

### SAST IaC

https://gitlab.com/greg/sastiac

```yaml
include:
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/ansible-lint.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/cfn-python-lint.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/cfn-scan.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/checkov.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/hadolint.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/kube-linter.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/puppet-lint.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/README.md"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/shellcheck.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/terrascan.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/tflint.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/tfsec.yml"
  - remote: "https://gitlab.com/greg/sastiac/-/raw/master/yamllint.yml"
```

### clamav/clamscan

Project: https://gitlab.com/greg/clamscan

```yaml
include:
  - remote: "https://gitlab.com/greg/clamscan/-/raw/master/templates/clamscan.gitlab-ci.yml"
```

### testssl

Project: https://gitlab.com/greg/testssl

```yaml
include:
  - remote: 'https://gitlab.com/greg/testssl/-/raw/master/templates/testssl.gitlab-ci.yml'

variables:
  TESTSSL_URL: "example.gitlab.com"
```



